﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Collections;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Diagnostics;

using MySql.Data.MySqlClient;

namespace OpenDentalSync
{
    public partial class frnMain : Form
    {
        private Hashtable m_htTablesOutofSync;
        private Hashtable m_htNoPRimaryKey;


        private bool m_bReportOnlyMode = true;

        private string m_strSourceConnectionString = "";
        private string m_strDestinationConnectionString = "";

        private int m_nMasterInsertCount = 0;
        private int m_nSlaveInsertCount = 0;

        private int m_nUpdateRecordsCount = 0;
        private int m_nUpdateTotalCount = 0;
        private int m_nMasterUpdateDateOnlyCount = 0;
        private int m_nSlaveUpdateDateOnlyCount = 0;
        private int m_nMasterUpdateNoSlaveDataCount = 0;
        private int m_nSlaveUpdateNoMasterDataCount = 0;
        private int m_nMasterSlaveDataDifferencesCount = 0;

        private int m_nMasterBlanks = 0;
        private int m_nSlaveBlanks = 0;

        private int m_nTotalRows = 0;
        private int m_nTotalIdenticalRows = 0;



        public frnMain()
        {
            InitializeComponent();
        }

        private void ResetValues()
        {
            m_nMasterInsertCount = 0;
            m_nSlaveInsertCount = 0;
            m_nUpdateRecordsCount = 0;
            m_nUpdateTotalCount = 0;
            m_nMasterUpdateDateOnlyCount = 0;
            m_nSlaveUpdateDateOnlyCount = 0;
            m_nMasterUpdateNoSlaveDataCount = 0;
            m_nSlaveUpdateNoMasterDataCount = 0;
            m_nMasterSlaveDataDifferencesCount = 0;
            m_nTotalRows = 0;
            m_nTotalIdenticalRows = 0;
            m_nMasterBlanks = 0;
            m_nSlaveBlanks = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string strSourceDatabase = "BARTON_Opendent";
            string strDestinationDatabase = "T1_Opendent";

            for (int i = 8; i <= 12; i++)
            {
                if (i != 2)
                {
                    strSourceDatabase = "BARTON_Opendent";
                    strDestinationDatabase = "T" + i + "_Opendent";

                    m_strSourceConnectionString = "SERVER=10.1.43.215;DATABASE=" + strSourceDatabase + ";UID=adm_jvoegele;PASSWORD=Austin2018!;SslMode=none;Command Timeout=0";
                    m_strDestinationConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strDestinationDatabase + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";


                    Hashtable htSourceTables = new Hashtable();
                    string strTableName = "";
                    string strPrimaryKey = "";


                    htSourceTables = GetTables(strSourceDatabase, m_strSourceConnectionString);
                    htSourceTables.Remove("procedurelog");
                    //htSourceTables.Add("procedurelog1", "ProcNum");
                    //htSourceTables.Add("procedurelog2", "ProcNum");

                    m_htTablesOutofSync = new Hashtable();
                    int nTablesOutofSyncCount = 0;
                    m_htNoPRimaryKey = new Hashtable();
                    int nNoPRimaryKeyCount = 0;



                    foreach (DictionaryEntry kvp in htSourceTables)
                    {

                        if (kvp.Key.ToString() != "procedurelog1")
                        {
                            Console.Write("123");
                        }
                        if (kvp.Key.ToString() != "procedurelog1")
                            ResetValues();

                        strTableName = kvp.Key.ToString();

                        strPrimaryKey = kvp.Value.ToString();
                        if (!string.IsNullOrEmpty(strPrimaryKey))
                        {
                            SyncDatabases(m_strSourceConnectionString, m_strDestinationConnectionString, strTableName, strPrimaryKey);
                            string strBody = "";
                            if (strTableName != "procedurelog2")
                            {
                                if (strTableName == "procedurelog1")
                                {
                                    strTableName = "procedurelog";
                                }
                                strBody = CreateStatusBody("", strSourceDatabase, strDestinationDatabase, strTableName, strPrimaryKey);
                            }
                            m_htTablesOutofSync.Add("nTablesOutofSyncCount" + nTablesOutofSyncCount.ToString(), strBody);
                            nTablesOutofSyncCount++;
                        }
                        else
                        {
                            m_htNoPRimaryKey.Add("nNoPRimaryKeyCount" + nNoPRimaryKeyCount.ToString(), strTableName);
                            nNoPRimaryKeyCount++;
                        }
                    }


                    string strStatusBody = "<p>Source,Destination,Table,PrimaryKey,DBDate,Total Rows, Identical Rows,A Not B,Rows to be Updated,B Not A,Total Cells Updated,A Date Only,B Date Only,Holes A to B, Holes B to A,Other Diffs,MasterBlanks,SlaveBlanks</p>";
                    string strTablesOutofsync = "";
                    foreach (DictionaryEntry kvp in m_htTablesOutofSync)
                    {
                        strTablesOutofsync = strTablesOutofsync + "<p>" + kvp.Value + "</p>";
                    }
                    SendStatusReport("jimmy@veicorporation.com;", strStatusBody + strTablesOutofsync);
                }
            }
        }

        private string CreateStatusBody(string strBody, string strMasterDatabase, string strSlaveDatabase, string strTableName, string strPrimaryKey)
        {
            string strNewBody = strMasterDatabase + "," + strSlaveDatabase + "," + strTableName + ","  + strPrimaryKey + "," + "6/1/2018" + "," + m_nTotalRows.ToString() + "," + m_nTotalIdenticalRows.ToString()  + "," + m_nSlaveInsertCount.ToString() + "," + m_nUpdateRecordsCount.ToString() + "," + m_nMasterInsertCount.ToString()  + "," + m_nUpdateTotalCount.ToString() + "," + m_nMasterUpdateDateOnlyCount.ToString() + "," + m_nSlaveUpdateDateOnlyCount.ToString() + "," + m_nMasterUpdateNoSlaveDataCount.ToString() + "," + m_nSlaveUpdateNoMasterDataCount.ToString() + "," + m_nMasterSlaveDataDifferencesCount.ToString() + "," + m_nMasterBlanks.ToString() + "," + m_nSlaveBlanks.ToString();


            //if (m_nMasterInsertCount == 0 && m_nSlaveInsertCount == 0 && m_nUpdateRecordsCount == 0)
            //{

            //    strNewBody = "<p><b>Table: " + strTableName + "</b></p>";
            //    strNewBody = strNewBody + "No Differences.";
            //}
            //else
            //{
            //    strNewBody = "<p><b>Table: " + strTableName + "</b></p>";
            //    strNewBody = strNewBody + "<p>Source Inserts =" + m_nMasterInsertCount.ToString() + "</p>";
            //    strNewBody = strNewBody + "<p>Destination Inserts =" + m_nSlaveInsertCount.ToString() + "</p>";
            //    strNewBody = strNewBody + "<p>Total Rows that need Updating =" + m_nUpdateRecordsCount.ToString() + "</p>";
            //    strNewBody = strNewBody + "<p>Total Cells that need Updating =" + m_nUpdateTotalCount.ToString() + "</p>";
            //    strNewBody = strNewBody + "<p>Date Only Updates (Source) =" + m_nMasterUpdateDateOnlyCount.ToString() + "</p>";
            //    strNewBody = strNewBody + "<p>Date Only Updates (Destination) =" + m_nSlaveUpdateDateOnlyCount.ToString() + "</p>";
            //    strNewBody = strNewBody + "<p>Data from Source to Destination (no destination data) =" + m_nMasterUpdateNoSlaveDataCount.ToString() + "</p>";
            //    strNewBody = strNewBody + "<p>Data from Destination to Source (no source data) =" + m_nSlaveUpdateNoMasterDataCount.ToString() + "</p>";
            //    strNewBody = strNewBody + "<p>Actual Data Differnces between source and destination =" + m_nMasterSlaveDataDifferencesCount.ToString() + "</p>";

            //}

            strNewBody = strBody + strNewBody;

            return strNewBody;
        }

        private Hashtable GetTables(string strDatabaseName, string strConnectionString)
        {

            MySqlConnection connection = new MySqlConnection(strConnectionString);
            connection.Open(); 

            string strSelect = "select TABLE_NAME from information_schema.tables where TABLE_TYPE = 'BASE TABLE' and TABLE_SCHEMA = '" + strDatabaseName + "'";

            MySql.Data.MySqlClient.MySqlCommand mySqlCommand = new MySqlCommand(strSelect, connection);
            MySql.Data.MySqlClient.MySqlDataReader reader = mySqlCommand.ExecuteReader();

            Hashtable htTables = new Hashtable();

            while (reader.Read())
            {
                string strTableName = reader["TABLE_NAME"].ToString();
                string strPrimaryKey = GetPrimaryKey(strTableName, strConnectionString);
                htTables.Add(strTableName, strPrimaryKey);
            }

            reader.Close();
            connection.Close();

            return htTables;

        }

        private string GetPrimaryKey(string strTableName, string strConnectionString)
        {
            MySqlConnection connection = new MySqlConnection(strConnectionString);
            connection.Open();

            string strSelect = "select distinct  column_name  from INFORMATION_SCHEMA.KEY_COLUMN_USAGE where TABLE_NAME = '" + strTableName + "'";
            MySql.Data.MySqlClient.MySqlCommand mySqlCommand = new MySqlCommand(strSelect, connection);
            MySql.Data.MySqlClient.MySqlDataReader reader = mySqlCommand.ExecuteReader();

            string strPrimaryKey = "";

            try
            {
                while (reader.Read())
                {
                    strPrimaryKey= reader["column_name"].ToString();
                }
            }catch
            {
                
            }finally
            {
                connection.Close();
                reader.Close();
            }

            return strPrimaryKey;
        }


        private void SyncDatabases(string strMasterConnectionString, string strSlaveConnectionString, string strTableName, string strPrimaryKey)
        {


            Debug.WriteLine(strTableName);
            DataTable DTTableSlave = GetSlaveRecords(strMasterConnectionString, strTableName, strSlaveConnectionString, strTableName, strPrimaryKey);
            DataTable DTTableMaster = GetMasterRecords(strMasterConnectionString, strTableName, strSlaveConnectionString, strTableName, strPrimaryKey);
            if (strTableName == "procedurelog1" || strTableName == "procedurelog2")
            {
                strTableName = "procedurelog";
            }

            Hashtable htMaster = new Hashtable();
            foreach (DataRow dr in DTTableMaster.Rows)
            {
                string strRecord = dr[strPrimaryKey].ToString();
                htMaster.Add(dr[strPrimaryKey].ToString(), dr);
            }

            Hashtable htSlave = new Hashtable();
            foreach (DataRow dr in DTTableSlave.Rows)
            {
                string strRecord = dr[strPrimaryKey].ToString();
                htSlave.Add(dr[strPrimaryKey].ToString(), dr);
            }

            //Sync from Slave to Master
            foreach (DataRow dr in DTTableSlave.Rows)
            {
                string strData = dr[strPrimaryKey].ToString();
                if (htMaster.Contains(strData))
                {
                    Console.Write("In Database");
                }
                else
                {
                    WriteDataRow(strMasterConnectionString, strTableName, dr);
                }
            }

            //Sync from Master to Slave
            foreach (DataRow dr in DTTableMaster.Rows)
            {
                string strData = dr[strPrimaryKey].ToString();
                if (htSlave.Contains(strData))
                {
                    Console.Write("In Database");
                }
                else
                {
                    WriteDataRow(strSlaveConnectionString, strTableName, dr);
                }
            }

            if (!m_bReportOnlyMode)
            {
                DTTableMaster = GetMasterRecords(strMasterConnectionString, strTableName, strSlaveConnectionString, strTableName, strPrimaryKey);
                DTTableSlave = GetSlaveRecords(strMasterConnectionString, strTableName, strSlaveConnectionString, strTableName, strPrimaryKey);


                htMaster = new Hashtable();
                foreach (DataRow dr in DTTableMaster.Rows)
                {
                    string strRecord = dr[strPrimaryKey].ToString();
                    htMaster.Add(dr[strPrimaryKey].ToString(), dr);
                }

                htSlave = new Hashtable();
                foreach (DataRow dr in DTTableSlave.Rows)
                {
                    string strRecord = dr[strPrimaryKey].ToString();
                    htSlave.Add(dr[strPrimaryKey].ToString(), dr);
                }
            }




            int BadRecordCount = 0;
            foreach (DataRow dr in DTTableMaster.Rows)
            {
                string strData = dr[strPrimaryKey].ToString();
                m_nTotalRows++;

                if (strData == "80356")
                {
                    Debug.Write("s");
                }

                DataRow drSlaveRecord = ((DataRow)htSlave[strData]);
                if (drSlaveRecord != null)
                {
                    if (!IsDataRowEqual(dr, drSlaveRecord, strMasterConnectionString, strTableName, strSlaveConnectionString, strTableName, strPrimaryKey))
                    {
                        BadRecordCount++;
                    }
                }
            }
            Debug.WriteLine("BadRecords = " + BadRecordCount);
        }

        private bool IsDataRowEqual(DataRow drMaster, DataRow drSlave, string strMasterConnectionString, string strMasterTableName, string strSlaveConnectionString, string strSlaveTableName, string strPrimaryKey)
        {
            bool bReturn = true;
            int nMasterColumnCount = drMaster.Table.Columns.Count;

            bool bTimeStampOnly = false;

            bool bUseMaster = true;

            Hashtable htFields = new Hashtable();

            string strMasterFull = string.Join("|", drMaster.ItemArray.Select(p => p.ToString()).ToArray());
            string strSlaveFull = string.Join("|", drSlave.ItemArray.Select(p => p.ToString()).ToArray());

            if (strMasterFull != strSlaveFull)
            {
                m_nUpdateRecordsCount++;
                for (int i = 0; i < nMasterColumnCount; i++)
                {
                    string strMaster = drMaster.ItemArray.GetValue(i).ToString();
                    string strSlave = drSlave.ItemArray.GetValue(i).ToString();

                    string strColumnName = drMaster.Table.Columns[i].ColumnName;

                    if (strMaster != strSlave)
                    {
                        m_nUpdateTotalCount++;
                        htFields.Add(strColumnName, strColumnName);
                        if (strColumnName == "DateTStamp")
                        {
                            DateTime dtMaster = Convert.ToDateTime(strMaster);
                            DateTime dtSlave = Convert.ToDateTime(strSlave);

                            if (dtSlave > dtMaster)
                            {
                                m_nSlaveUpdateDateOnlyCount++;
                                bUseMaster = false;
                            }
                            else
                                m_nMasterUpdateDateOnlyCount++;

                            bTimeStampOnly = true;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(strMaster))
                            {
                                m_nSlaveUpdateNoMasterDataCount++;
                                UpdateDataRow(strMasterConnectionString, strMasterTableName, drSlave, htFields, strPrimaryKey);
                            }
                            else if (string.IsNullOrEmpty(strSlave))
                            {
                                m_nMasterUpdateNoSlaveDataCount++;
                                UpdateDataRow(strSlaveConnectionString, strSlaveTableName, drMaster, htFields, strPrimaryKey);
                            }
                            else
                            {
                                m_nMasterSlaveDataDifferencesCount++;
                            }
                            bReturn = false;
                        }

                    }

                    if (drSlave.ItemArray.GetValue(i).Equals(System.DBNull.Value) && drMaster.ItemArray.GetValue(i) == "")
                    {
                        if (!htFields.Contains(strColumnName))
                        {
                            htFields.Add(strColumnName, strColumnName);
                            UpdateDataRow(strMasterConnectionString, strMasterTableName, drSlave, htFields, strPrimaryKey);
                            m_nMasterBlanks++;
                        }
                    }

                    if (drMaster.ItemArray.GetValue(i).Equals(System.DBNull.Value) && drSlave.ItemArray.GetValue(i) == "")
                    {
                        if (!htFields.Contains(strColumnName))
                        {
                            htFields.Add(strColumnName, strColumnName);
                            UpdateDataRow(strSlaveConnectionString, strSlaveTableName, drMaster, htFields, strPrimaryKey);
                            m_nSlaveBlanks++;
                        }
                    }
                }

                if (bTimeStampOnly)
                {
                    Debug.Write("123");

                    if (bUseMaster)
                        UpdateDataRow(strSlaveConnectionString, strSlaveTableName, drMaster, htFields, strPrimaryKey);
                    else
                        UpdateDataRow(strMasterConnectionString, strMasterTableName, drSlave, htFields, strPrimaryKey);
                }
            }else
            {
                m_nTotalIdenticalRows++;
            }
            return bReturn;
        }




        private DataTable GetMasterRecords(string strMasterConnectionString, string strMasterTableName, string strSlaveConnectionString, string strSlaveTableName, string strPrimaryKey)
        {
            MySqlConnection connectionMaster = new MySqlConnection(strMasterConnectionString);
            connectionMaster.Open();

            MySql.Data.MySqlClient.MySqlCommand mySqlCommandMaster = null;

            if (strMasterTableName == "procedurelog1")
                mySqlCommandMaster = new MySqlCommand("SELECT * FROM " + "procedurelog" + " where ProcNum <= 43017248", connectionMaster);
            else if (strMasterTableName == "procedurelog2")
                mySqlCommandMaster = new MySqlCommand("SELECT * FROM " + "procedurelog" + " where ProcNum > 43017248", connectionMaster);
            else
                mySqlCommandMaster = new MySqlCommand("SELECT * FROM " + strMasterTableName, connectionMaster);

            MySql.Data.MySqlClient.MySqlDataReader readerMaster = mySqlCommandMaster.ExecuteReader();

            System.Data.DataTable DTTableMaster = new System.Data.DataTable(strMasterTableName);
            for (int i = 0; i < readerMaster.FieldCount; i++)
            {
                string strType = readerMaster.GetFieldType(i).ToString();
                if (!strType.Contains("TimeSpan"))
                {
                    DTTableMaster.Columns.Add(readerMaster.GetName(i), readerMaster.GetFieldType(i));
                }
                else
                {
                    DTTableMaster.Columns.Add(readerMaster.GetName(i), Type.GetType("System.String"));
                }
            }

            DTTableMaster.PrimaryKey = new DataColumn[] { DTTableMaster.Columns[strPrimaryKey] };

            while (readerMaster.Read())
            {
                DataRow dr = DTTableMaster.NewRow();

                for (int i = 0; i < readerMaster.FieldCount; i++)
                {
                    try
                    {
                        dr[DTTableMaster.Columns[i]] = readerMaster.GetValue(i);
                    }
                    catch (Exception ex)
                    {
                        Console.Write("MySQL Error- " + ex.Message);
                    }
                }
                DTTableMaster.Rows.Add(dr);
            }

            connectionMaster.Close();

            readerMaster.Close();

            int nCountMaster = DTTableMaster.Rows.Count;

            return DTTableMaster;
        }

        private DataTable GetSlaveRecords(string strMasterConnectionString, string strMasterTableName, string strSlaveConnectionString, string strSlaveTableName, string strPrimaryKey)
        {

            MySqlConnection connectionSlave = new MySqlConnection(strSlaveConnectionString);
            connectionSlave.Open();
            MySql.Data.MySqlClient.MySqlCommand mySqlCommandSlave = null;

            if (strMasterTableName == "procedurelog1")
                mySqlCommandSlave = new MySqlCommand("SELECT * FROM " + "procedurelog" + " where ProcNum <= 43017248" , connectionSlave);
            else if (strMasterTableName == "procedurelog2")
                mySqlCommandSlave = new MySqlCommand("SELECT * FROM " + "procedurelog" + " where ProcNum > 43017248", connectionSlave);
            else
                mySqlCommandSlave = new MySqlCommand("SELECT * FROM " + strSlaveTableName, connectionSlave);

            MySql.Data.MySqlClient.MySqlDataReader readerSlave = mySqlCommandSlave.ExecuteReader();

            System.Data.DataTable DTTableSlave = new System.Data.DataTable(strSlaveTableName);
            for (int i = 0; i < readerSlave.FieldCount; i++)
            {
                string strType = readerSlave.GetFieldType(i).ToString();
                if (!strType.Contains("TimeSpan"))
                {
                    DTTableSlave.Columns.Add(readerSlave.GetName(i), readerSlave.GetFieldType(i));
                }
                else
                {
                    DTTableSlave.Columns.Add(readerSlave.GetName(i), Type.GetType("System.String"));
                }

            }

            DTTableSlave.PrimaryKey = new DataColumn[] { DTTableSlave.Columns[strPrimaryKey] };

            while (readerSlave.Read())
            {
                DataRow dr = DTTableSlave.NewRow();

                for (int i = 0; i < readerSlave.FieldCount; i++)
                {
                    try
                    {
                        dr[DTTableSlave.Columns[i]] = readerSlave.GetValue(i);
                    }
                    catch (Exception ex)
                    {
                        Console.Write("MySQL Error- " + ex.Message);
                    }
                }
                DTTableSlave.Rows.Add(dr);
            }

            int nCountSlave = DTTableSlave.Rows.Count;

            connectionSlave.Close();

            readerSlave.Close();

            return DTTableSlave;
        }




        private void CopyTable(string strFromConnectionString, string strFromTableName, string strToConnectionString, string strToTableName, string UseMySQL = "False", bool bTruncateTable = true)
        {
            MySqlConnection connectionFrom = new MySqlConnection(strFromConnectionString);
            connectionFrom.Open();

            MySqlConnection connectionTo = new MySqlConnection(strToConnectionString);
            connectionTo.Open();

            //            MySql.Data.MySqlClient.MySqlCommand mySqlCommand = new MySqlCommand("CREATE TABLE appointment_new LIKE appointment;", connectionFrom);


            MySql.Data.MySqlClient.MySqlCommand mySqlCommand = new MySqlCommand("INSERT appointment_new SELECT * FROM appointment;", connectionFrom);
            mySqlCommand.ExecuteNonQuery();


            //MySql.Data.MySqlClient.MySqlCommand mySqlCommand = new MySqlCommand("SELECT * FROM " + strFromTableName, connectionFrom);
            //MySql.Data.MySqlClient.MySqlDataReader reader = mySqlCommand.ExecuteReader();

            //bool bCreateInsertString = true;
            //string strSQLInsertParamters = string.Empty;
            //string strSQLInsertValues = string.Empty;
            //while (reader.Read())
            //{
            //    for (int i = 0; i < reader.FieldCount; i++)
            //    {
            //        try
            //        {
            //            if (bCreateInsertString)
            //            {
            //                if (string.IsNullOrEmpty(strSQLInsertParamters))
            //                    strSQLInsertParamters = reader.GetName(i);
            //                else
            //                    strSQLInsertParamters = strSQLInsertParamters + "," + reader.GetName(i);
            //            }

            //            if (string.IsNullOrEmpty(strSQLInsertValues))
            //                strSQLInsertValues = strSQLInsertValues + "'" + reader.GetValue(i).ToString().Replace("'", "''") + "'";
            //            else
            //                strSQLInsertValues = strSQLInsertValues + "," + "'" + reader.GetValue(i).ToString().Replace("'","''") + "'"; 
            //        }
            //        catch (Exception ex)
            //        {
            //            Console.Write("MySQL Error- " + ex.Message);
            //        }
            //    }
            //    bCreateInsertString = false;
            //    MySql.Data.MySqlClient.MySqlCommand cmdInsert = new MySqlCommand("INSERT INTO appointment_test(" + strSQLInsertParamters + ") VALUES(" + strSQLInsertValues + ")", connectionTo);
            //    cmdInsert.ExecuteNonQuery();

            //    strSQLInsertValues = "";
            //}

            //reader.Close();
        }

        private void UpdateDataRow(string strDestinationConnectionString, string strDestinationTableName, DataRow drSource, Hashtable htFields, string strPrimaryKey)
        {


            string strPrimaryKeyValue = drSource[strPrimaryKey].ToString();

            string strSQLUpdateParamters = "";

            for (int i = 0; i < drSource.Table.Columns.Count; i++)
            {
                if (htFields.Contains(drSource.Table.Columns[i].ColumnName))
                {
                    string strValue = string.Empty;
                    if (drSource.Table.Columns[i].DataType.Name == "DateTime")
                    {
                        DateTime dt = Convert.ToDateTime(drSource.ItemArray[i].ToString());
                        strValue = "'" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                    }
                    else
                    {
                        if (drSource.ItemArray.GetValue(i).Equals(System.DBNull.Value))
                            strValue = "null";
                        else
                            strValue = "'" + drSource.ItemArray[i].ToString().ToString().Replace("'", "''") + "'";
                    }

                    if (string.IsNullOrEmpty(strSQLUpdateParamters))
                        strSQLUpdateParamters = drSource.Table.Columns[i].ColumnName + "=" + strValue;
                    else
                        strSQLUpdateParamters = strSQLUpdateParamters + "," + drSource.Table.Columns[i].ColumnName + "=" + strValue;
                }
            }

            if (!m_bReportOnlyMode)
            {
                MySqlConnection connectionDestination = new MySqlConnection(strDestinationConnectionString);
                connectionDestination.Open();

                MySql.Data.MySqlClient.MySqlCommand cmdUpdate = new MySqlCommand("Update " + strDestinationTableName + " Set " + strSQLUpdateParamters + " where " + strPrimaryKey + " = " + strPrimaryKeyValue, connectionDestination);
                cmdUpdate.ExecuteNonQuery();

                connectionDestination.Close();
            }
    }

        private void WriteDataRow(string strMasterConnectionString, string strMasterTableName, DataRow dr)
        {
            string strSQLInsertParamters = "";
            string strSQLInsertValues = "";

            for (int i = 0; i < dr.Table.Columns.Count; i++)
            {
                if (string.IsNullOrEmpty(strSQLInsertParamters))
                    strSQLInsertParamters = dr.Table.Columns[i].ColumnName;
                else
                    strSQLInsertParamters = strSQLInsertParamters + "," + dr.Table.Columns[i].ColumnName;

                if (string.IsNullOrEmpty(strSQLInsertValues))
                    strSQLInsertValues = strSQLInsertValues + "'" + dr.ItemArray[i].ToString().ToString().Replace("'", "''") + "'";
                else
                {
                    if (dr.Table.Columns[i].DataType.Name == "DateTime")
                    {
                        DateTime dt = Convert.ToDateTime(dr.ItemArray[i].ToString());
                        strSQLInsertValues = strSQLInsertValues + "," + "'" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                    }
                    else
                        strSQLInsertValues = strSQLInsertValues + "," + "'" + dr.ItemArray[i].ToString().ToString().Replace("'", "''") + "'";
                }
            }

            if (!m_bReportOnlyMode)
            {
                MySqlConnection connectionMaster = new MySqlConnection(strMasterConnectionString);
                connectionMaster.Open();

                MySql.Data.MySqlClient.MySqlCommand cmdInsert = new MySqlCommand("INSERT INTO " + strMasterTableName + " (" + strSQLInsertParamters + ") VALUES(" + strSQLInsertValues + ")", connectionMaster);
                cmdInsert.ExecuteNonQuery();

                connectionMaster.Close();
            }

            if (strMasterConnectionString == m_strSourceConnectionString)
                m_nMasterInsertCount++;

            if (strMasterConnectionString == m_strDestinationConnectionString)
                m_nSlaveInsertCount++;

        }

        public void SendStatusReport(string strEmailAddresses, string strBody)
        {
            cMail mail = new cMail();

            string strSubject = "Open Dental Synchronization Report - Tables out of sync";
            string strUserName = "sdfbuild@stdavidsfoundation.org";
            string strPassword = "Yesterday!";



            mail.SendStatus(strSubject, strBody, strEmailAddresses, strUserName, strPassword);
        }

    }
}
