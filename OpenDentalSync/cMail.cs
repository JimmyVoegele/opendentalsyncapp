﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Net.Mail;

namespace OpenDentalSync
{
    public class cMail
    {
        public bool SendStatus(string strSubject, string strMessage, string strEmailAddresses, string MailUserName, string MailPassword)
        {
            try
            {
                MailMessage mail = new MailMessage();
                MailAddress from = new MailAddress(MailUserName);

                mail.From = from;

                AddEmailAddresses(mail, strEmailAddresses);

                mail.Subject = strSubject;


                mail.IsBodyHtml = true;
                mail.Body = strMessage;

                SmtpClient client = new SmtpClient();
                client.Host = "smtp.office365.com";
                client.Credentials = new System.Net.NetworkCredential(MailUserName, MailPassword);
                client.Port = 587;
                client.EnableSsl = true;
                client.Send(mail);
            }
            catch
            {
                return false;
            }
            return true;
        }

        static void AddEmailAddresses(MailMessage mail, string strSummaryEmailAddresses)
        {
            string[] strAddresses = strSummaryEmailAddresses.Split(';');
            foreach (string strTo in strAddresses)
            {
                MailAddress to = new MailAddress(strTo);
                mail.To.Add(to);
            }
        }
    }
}
